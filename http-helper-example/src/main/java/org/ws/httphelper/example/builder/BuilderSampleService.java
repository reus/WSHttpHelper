package org.ws.httphelper.example.builder;


public interface BuilderSampleService {

    /**
     * 根据URL保存URL中所有图片
     * @param url
     */
    void searchAndDownload(String url);

}
