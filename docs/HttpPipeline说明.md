# HttpPipeline

HttpPipeline是一个流水线，每一个Temple都有一个自定义配置的Pipeline，在每次执行请求的时候对输入进行处理生成RequestData以便HttpFetcher生成Request执行请求，在响应后对输出执行处理，可以自动解析数据、保存数据、执行二次请求或其他操作。

HttpPipeline是针对Temple的配置，所以每一类请求对应的Temple也就对应一组相同的输入输出的处理Handler。如解析类：针对REST接口的POST请求，可能需要将输入参数序列化为JSON作为请求的Body，并将响应的JSON解析为对象；保存类：针对流媒体进行保存，请求后将响应的流数据存储为文件，这个流可以是图片、PDF、ZIP、MP3、MP4等；

### Pipeline中Handler执行过程

HttpPipeline内包含两大类Handler分别为RequestHandler（执行Http请求前处理器），ResponseHandler（执行Http请求后处理器）。在执行request处理的时候会从head开始向tail逐个执行所有的RequestHandler生成RequestData；在执行response处理的时候会从tail开始向head逐个执行所有的ResponseHandler；执行过程见下图：

<img src="assets/HttpPipeline说明/Pipeline-Runtime.png" style="zoom:80%;" />

### RequestHandler和ResponseHandler执行的优先级

HttpPipeline是一个可以支持双向优先级的列表。HttpPipeline内部结构是由一组包含了Handler的Context组成的双向链表。双向链表的头尾为两个内置的Context分别是HeadContext和TailContext。Handler执行的优先级通过头尾挨个执行的顺序确定，在向Pipeline中插入Handler的时候根据@HandlerOrder定义的级别和值决定HandlerContext插入到Pipeline中的位置，插入的位置由HandlerContext中的计算的order值决定，HandlerContext根据order从Head到Tail按照从小到大的顺序排列。

在执行Request操作的时候是从Head向Tail挨个执行，约靠近Head约优先执行。

在执行Response操作的时候是从Tail向Head挨个执行，约靠近Tail约优先执行。

<img src="assets/HttpPipeline说明/Pipeline-Flow.png" style="zoom:80%;" />

##### @HandlerOrder说明

* level：约定无论是RequestHandler还是ResponseHandler，SYSTEM基本的Handler优先执行,通过高位标识实现。
* value：相同的level的优先级
  * RequestHandler：value越小越靠进Head，越优先执行，默认为`0x00008000`（User级别的最小值）
  * ResponseHandler：value越大越靠进Tail，越优先执行，默认为`0x00010000`（User基本的最小值）

##### HandlerContext中order计算

* RequestHandler：值的范围在0~0x0000FFFFF，值小，靠近Head，其中System级别更小更优先;
* ResponseHandler：值的范围在0x00010000~0x7FFF0000，值大，靠近Tail，其中System级别更大更优先；

<img src="assets/HttpPipeline说明/HandlerContext-order.png" style="zoom:90%;" />

### Pipeline类图

<img src="assets/HttpPipeline说明/Pipeline.png" style="zoom:80%;" />


