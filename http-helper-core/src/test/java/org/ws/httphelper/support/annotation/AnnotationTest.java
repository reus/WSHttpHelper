package org.ws.httphelper.support.annotation;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ws.httphelper.builder.HttpHelperBuilder;
import org.ws.httphelper.builder.annotation.Delete;
import org.ws.httphelper.builder.annotation.Get;
import org.ws.httphelper.builder.annotation.HttpRequest;
import org.ws.httphelper.exception.BuildException;
import org.ws.httphelper.model.config.RequestConfig;
import org.ws.httphelper.model.http.ResponseFuture;
import org.ws.httphelper.servlet.JettyServlet;
import org.ws.httphelper.support.pipeline.handler.response.SaveFileHandler;

import java.io.File;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/** 
* AnnBuildUtils Tester. 
* 
* @author <Authors name> 
* @since <pre>十一月 15, 2020</pre> 
* @version 1.0 
*/ 
public class AnnotationTest {

    private static Logger log = LoggerFactory.getLogger(AnnotationTest.class.getName());

    private static JettyServlet jettyServlet = new JettyServlet(8888);

    private MyApiByOkHttp myHttpOperation;

    @Before
    public void before() throws Exception {
        jettyServlet.start();
        myHttpOperation = HttpHelperBuilder.builder().builderByAnn(MyApiByOkHttp.class);
    }

    /**
    *
    * Method: makeClientConfig(HttpClient httpClientAnn)
    *
    */
    @Test
    public void testMakeClientConfig() throws Exception {
        HttpRequest annotation = MyApiByOkHttp.class.getAnnotation(HttpRequest.class);
        RequestConfig requestConfig = AnnotationUtils.makeRequestConfig(AnnotationUtils.getRequestConfigMap(annotation));

        Assert.assertNotNull(requestConfig);
        log.info(JSON.toJSONString(requestConfig, SerializerFeature.PrettyFormat));
    }

    /**
    *
    * Method: makeRequestConfig(Get get, HttpRequest httpRequest)
    *
    */
    @Test
    public void testMakeRequestConfigForGetHttpRequest() throws Exception {

        Method method = MyApiByOkHttp.class.getMethod("getHtml", String.class);
        Get annotation1 = method.getAnnotation(Get.class);
        RequestConfig requestConfig = AnnotationUtils.makeRequestConfig(AnnotationUtils.getRequestConfigMap(annotation1));

        Assert.assertNotNull(requestConfig);
        log.info(JSON.toJSONString(requestConfig, SerializerFeature.PrettyFormat));

        Method method2 = MyApiByOkHttp.class.getMethod("deleteForFuture", String.class,String.class);
        Delete annotation2 = method2.getAnnotation(Delete.class);
        RequestConfig requestConfig2 = AnnotationUtils.makeRequestConfig(AnnotationUtils.getRequestConfigMap(annotation2));

        Assert.assertNotNull(requestConfig2);
        log.info(JSON.toJSONString(requestConfig2, SerializerFeature.PrettyFormat));
    }

    @Test
    public void testGet() throws BuildException {

        ResponseFuture future = myHttpOperation.getHtml("/test");

        Assert.assertTrue(future.isSuccess());
        log.info(JSON.toJSONString(future, SerializerFeature.PrettyFormat));

        future = myHttpOperation.getTest();
        Assert.assertTrue(future.isSuccess());
        log.info(JSON.toJSONString(future, SerializerFeature.PrettyFormat));
    }

    @Test
    public void testDelete() throws BuildException {

        ResponseFuture future = myHttpOperation.deleteForFuture("/test","id");

        Assert.assertTrue(future.isSuccess());
        log.info(JSON.toJSONString(future, SerializerFeature.PrettyFormat));

        ResponseFuture future2 = myHttpOperation.deleteByWebForFuture("/test","id");

        Assert.assertTrue(future2.isSuccess());
        log.info(JSON.toJSONString(future2, SerializerFeature.PrettyFormat));

    }

    @Test
    public void testPostJson()throws Exception {

        Map<String,String> data = new HashMap<>();
        data.put("key1","value1");
        data.put("key2","value2");
        data.put("key3","value3");

        ResponseFuture postJsonFuture = myHttpOperation.postJson("/test", data);
        Assert.assertTrue(postJsonFuture.isSuccess());
        log.info(JSON.toJSONString(postJsonFuture, SerializerFeature.PrettyFormat));

        Map map = myHttpOperation.postJsonToGeneric("/test", data, Map.class);
        log.info(JSON.toJSONString(map, SerializerFeature.PrettyFormat));

        Map map2 = myHttpOperation.postJsonToMap("/test", data, Map.class);
        log.info(JSON.toJSONString(map2, SerializerFeature.PrettyFormat));

        CountDownLatch countDownLatch = new CountDownLatch(1);
        myHttpOperation.postJsonCallback("/test", data, responseFuture -> {
            log.info(JSON.toJSONString(responseFuture, SerializerFeature.PrettyFormat));
            countDownLatch.countDown();
        });
        countDownLatch.await();

        String body = myHttpOperation.postJsonForBody("/test", data);
        log.info("body:{}",body);
    }

    @Test
    public void testDownload()throws Exception{
        Path path = Paths.get(JettyServlet.class.getResource("/").toURI());
        String rootPath = path.toString() + "/download";
        myHttpOperation.pipeline().addLast(new SaveFileHandler(rootPath));
        String fileName = "/HttpClassDiagram.png";
        byte[] bytes = myHttpOperation.downloadImg(fileName);
        Assert.assertNotNull(bytes);
    }

    public void testResponseEntity()throws Exception{

    }
} 
