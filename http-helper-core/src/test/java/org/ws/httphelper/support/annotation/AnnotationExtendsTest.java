package org.ws.httphelper.support.annotation;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ws.httphelper.builder.HttpHelperBuilder;
import org.ws.httphelper.exception.BuildException;
import org.ws.httphelper.model.http.ResponseFuture;
import org.ws.httphelper.servlet.JettyServlet;
import org.ws.httphelper.support.pipeline.handler.response.SaveFileHandler;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/** 
* AnnBuildUtils Tester. 
* 
* @author <Authors name> 
* @since <pre>十一月 15, 2020</pre> 
* @version 1.0 
*/ 
public class AnnotationExtendsTest {

    private static Logger log = LoggerFactory.getLogger(AnnotationExtendsTest.class.getName());

    private static JettyServlet jettyServlet = new JettyServlet(8888);

    private MyExtendsApi myExtendsApi;

    private String path = "/test";

    @Before
    public void before() throws Exception {
        jettyServlet.start();
        myExtendsApi = HttpHelperBuilder.builder().builderByAnn(MyExtendsApi.class);
    }


    @Test
    public void testGet() throws BuildException {

        ResponseFuture future = myExtendsApi.request(path);

        Assert.assertTrue(future.isSuccess());
        log.info(JSON.toJSONString(future, SerializerFeature.PrettyFormat));

        future = myExtendsApi.getHtml(path);

        Assert.assertTrue(future.isSuccess());
        log.info(JSON.toJSONString(future, SerializerFeature.PrettyFormat));

        future = myExtendsApi.getTest();
        Assert.assertTrue(future.isSuccess());
        log.info(JSON.toJSONString(future, SerializerFeature.PrettyFormat));
    }

    @Test
    public void testDelete() throws BuildException {

        ResponseFuture future = myExtendsApi.deleteForFuture(path,"id");

        Assert.assertTrue(future.isSuccess());
        log.info(JSON.toJSONString(future, SerializerFeature.PrettyFormat));

        ResponseFuture future2 = myExtendsApi.deleteByWebForFuture(path,"id");

        Assert.assertTrue(future2.isSuccess());
        log.info(JSON.toJSONString(future2, SerializerFeature.PrettyFormat));

    }

    @Test
    public void testPostJson()throws Exception {

        Map<String,String> data = new HashMap<>();
        data.put("key1","value1");
        data.put("key2","value2");
        data.put("key3","value3");

        ResponseFuture postJsonFuture = myExtendsApi.postJson(path, data);
        Assert.assertTrue(postJsonFuture.isSuccess());
        log.info(JSON.toJSONString(postJsonFuture, SerializerFeature.PrettyFormat));

        Map map = myExtendsApi.postJsonToGeneric(path, data, Map.class);
        log.info(JSON.toJSONString(map, SerializerFeature.PrettyFormat));

        Map map2 = myExtendsApi.postJsonToMap(path, data, Map.class);
        log.info(JSON.toJSONString(map2, SerializerFeature.PrettyFormat));

        CountDownLatch countDownLatch = new CountDownLatch(1);
        myExtendsApi.postJsonCallback(path, data, responseFuture -> {
            log.info(JSON.toJSONString(responseFuture, SerializerFeature.PrettyFormat));
            countDownLatch.countDown();
        });
        countDownLatch.await();

        String body = myExtendsApi.postJsonForBody(path, data);
        log.info("body:{}",body);
    }

    @Test
    public void testDownload()throws Exception{
        Path path = Paths.get(JettyServlet.class.getResource("/").toURI());
        String rootPath = path.toString() + "/download";
        myExtendsApi.pipeline().addLast(new SaveFileHandler(rootPath));
        String fileName = "/HttpClassDiagram.png";
        ResponseFuture request = myExtendsApi.request(fileName);
        Assert.assertTrue(request.isSuccess());

    }

} 
