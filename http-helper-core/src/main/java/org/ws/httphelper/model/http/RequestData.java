package org.ws.httphelper.model.http;

import com.google.common.collect.Maps;
import org.ws.httphelper.common.Constant;

import java.util.Map;

public class RequestData {
    private String url;
    private HttpMethod method = HttpMethod.GET;
    private Map<String, String> headers;
    private ContentType contentType = ContentType.EMPTY;
    private Object body;
    private String charset = Constant.UTF_8;

    public RequestData() {
    }

    public RequestData(String url, HttpMethod method) {
        this.url = url;
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getHeaders() {
        if(headers == null){
            headers = Maps.newHashMap();
        }
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }
}
