package org.ws.httphelper.template;

import org.ws.httphelper.builder.ClientConfigBuilder;
import org.ws.httphelper.model.config.ClientConfig;
import org.ws.httphelper.model.http.ClientType;

/**
 * 默认Client配置
 */
public class DefaultClientConfig {

    private DefaultClientConfig() {

    }

    /**
     * OkHttpClient适用于API调用,不开启Cookie和followRedirects.
     * @param threadNumber 异步线程数
     * @param timeout 超时时间
     * @return
     */
    public static ClientConfig okHttpClientConfig(int threadNumber, int timeout){
        return okHttpClientConfig(threadNumber,timeout,false);
    }

    /**
     * OkHttpClient适用于API调用,不开启followRedirects.
     * @param threadNumber 异步线程数
     * @param timeout 超时时间
     * @param privateClient 是否私有客户端：true=每一个HttpHelper使用私有Client；false=从FetcherPool中获取共享Client。
     * @return
     */
    public static ClientConfig okHttpClientConfig(int threadNumber, int timeout, boolean privateClient){
        return okHttpClientConfig(threadNumber,timeout,false,privateClient);
    }

    /**
     * OkHttpClient适用于API调用,不开启Cookie.
     * @param threadNumber 异步线程数
     * @param timeout 超时时间
     * @param followRedirects 自动跳转
     * @param privateClient 私有客户端
     * @return
     */
    public static ClientConfig okHttpClientConfig(int threadNumber, int timeout,
                                                  boolean followRedirects, boolean privateClient){
        return ClientConfigBuilder.builder()
                .threadNumber(threadNumber)
                .timeout(timeout)
                .clientType(ClientType.OK_HTTP_CLIENT)
                .followRedirects(followRedirects)
                .privateClient(privateClient)
                .enableSsl(true)
                .enableCookie(false)
                .build();
    }

    /**
     * WebClient适用于执行JS渲染的HTML.
     * 不开启cookie.
     * @param threadNumber 异步线程数
     * @param timeout 超时时间
     * @return
     */
    public static ClientConfig webClientConfig(int threadNumber, int timeout){
        return webClientConfig(threadNumber,timeout,false);
    }

    /**
     * WebClient配置
     * @param threadNumber
     * @param timeout
     * @param privateClient
     * @return
     */
    public static ClientConfig webClientConfig(int threadNumber, int timeout, boolean privateClient){
        return webClientConfig(threadNumber,timeout,true,privateClient);
    }

    /**
     * WebClient适用于执行JS渲染的HTML
     * @param threadNumber
     * @param timeout
     * @param enableCookie
     * @return
     */
    public static ClientConfig webClientConfig(int threadNumber, int timeout,
                                               boolean enableCookie, boolean privateClient){
        return ClientConfigBuilder.builder()
                .threadNumber(threadNumber)
                .timeout(timeout)
                .clientType(ClientType.WEB_CLIENT)
                .enableCookie(enableCookie)
                .privateClient(privateClient)
                .enableSsl(true)
                .followRedirects(true)
                .build();
    }

}
