package org.ws.httphelper.support.parser.field.operator;

import org.ws.httphelper.model.field.ParseField;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexParseOperator extends AbstractParseOperator<String> {

    private Pattern pattern = null;
    private Matcher matcher = null;

    private String clearHtml(String html){
        return html.replaceAll("\n|\r","")
                .replaceAll("\t"," ")
                .replaceAll("\\s+"," ");
    }

    @Override
    public String parseString(String body, ParseField parseField) throws Exception {
        body = clearHtml(body);
        return evaluateString(parseField,body);
    }

    @Override
    public List parseList(String body, ParseField listFieldInfo) throws Exception {
        body = clearHtml(body);
        return evaluateList(listFieldInfo,body);
    }

    @Override
    public Map parseMap(String body, ParseField mapFieldInfo) throws Exception {
        body = clearHtml(body);
        return evaluateMap(mapFieldInfo,body);
    }

    @Override
    protected String evaluateString(ParseField parseField, String source) throws Exception {
        String parseExpression = parseField.getParseExpression();
        pattern = Pattern.compile(parseExpression);
        matcher = pattern.matcher(source);
        if(matcher.find()){
            String s = matcher.group(1);
            // 清除HTML
            return s.replaceAll("<[^>]+>","").trim();
        }
        return null;
    }

    @Override
    protected List evaluateList(ParseField parseField, String source) throws Exception {
        String parseExpression = parseField.getParseExpression();
        pattern = Pattern.compile(parseExpression);
        matcher = pattern.matcher(source);
        List<String> htmlList = new ArrayList<>();
        while (matcher.find()) {
            htmlList.add(matcher.group(1));
        }
        List resultList = new ArrayList();
        if(parseField.hasChild()){
            List<ParseField> childFieldList = parseField.getChildFieldInfoList();
            for(String html:htmlList){
                Map map = new HashMap();
                evaluateChild(childFieldList,html,map);
                resultList.add(map);
            }
            return resultList;
        }
        else {
            // String列表项
            return htmlList;
        }
    }

    @Override
    protected Map evaluateMap(ParseField parseField, String source) throws Exception {
        String html = parseString(source, parseField);
        // 对象必须有下级属性
        if(parseField.hasChild()){
            Map map = new HashMap();
            List<ParseField> childFieldList = parseField.getChildFieldInfoList();
            evaluateChild(childFieldList,html,map);
            return map;
        }
        return null;
    }
    
}
