package org.ws.httphelper.support.parser.field;

import org.ws.httphelper.core.parser.ParseOperator;
import org.ws.httphelper.exception.ParseException;
import org.ws.httphelper.model.field.ParseField;
import org.ws.httphelper.model.field.ParseFieldType;

public class FieldParser {

    public Object parse(String body, ParseField fieldInfo) throws ParseException {
        try {
            ParseOperator parseOperator = ParseOperatorFactory.getInstance().getParseOperator(fieldInfo.getExpressionType());
            if(parseOperator == null){
                throw new ParseException("["+fieldInfo.getExpressionType()+"] can not found ParseOperator.");
            }
            Object value = null;
            if (fieldInfo.getFieldType() == ParseFieldType.STRING) {
                value = parseOperator.parseString(body, fieldInfo);
            } else if (fieldInfo.getFieldType() == ParseFieldType.LIST) {
                value = parseOperator.parseList(body,fieldInfo);
            } else if (fieldInfo.getFieldType() == ParseFieldType.OBJECT) {
                value = parseOperator.parseMap(body,fieldInfo);
            }
            return value;
        }
        catch (Exception e){
            throw new ParseException(e.getMessage(),e);
        }
    }
}
