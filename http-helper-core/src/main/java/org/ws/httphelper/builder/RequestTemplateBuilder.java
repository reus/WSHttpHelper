package org.ws.httphelper.builder;

import org.ws.httphelper.model.config.ClientConfig;
import org.ws.httphelper.model.config.RequestConfig;
import org.ws.httphelper.model.template.RequestTemplate;

public final class RequestTemplateBuilder {
    private RequestConfigBuilder requestConfigBuilder;
    private ClientConfigBuilder clientConfigBuilder;

    private RequestTemplate requestTemplate;

    private RequestTemplateBuilder() {
        requestTemplate = new RequestTemplate();
        this.clientConfigBuilder = ClientConfigBuilder.builder(this);
    }

    public static RequestTemplateBuilder builder() {
        return new RequestTemplateBuilder();
    }

    public ClientConfigBuilder buildClientConfig() {
        return this.clientConfigBuilder;
    }

    public RequestTemplateBuilder clientConfig(ClientConfig clientConfig) {
        requestTemplate.setClientConfig(clientConfig);
        return this;
    }

    public RequestConfigBuilder buildRequestConfig() {
        if(this.requestConfigBuilder == null){
            this.requestConfigBuilder = RequestConfigBuilder.builder(this);
        }
        return this.requestConfigBuilder;
    }

    public RequestConfigBuilder buildRequestConfig(RequestConfig patent) {
        if(this.requestConfigBuilder == null){
            this.requestConfigBuilder = RequestConfigBuilder.builder(patent,this);
        }
        return this.requestConfigBuilder;
    }

    public RequestTemplateBuilder requestConfig(RequestConfig requestConfig) {
        requestTemplate.setRequestConfig(requestConfig);
        return this;
    }

    public RequestTemplate build() {

        return requestTemplate;
    }
}
