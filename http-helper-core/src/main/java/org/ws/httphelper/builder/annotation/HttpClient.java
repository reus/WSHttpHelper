package org.ws.httphelper.builder.annotation;

import org.ws.httphelper.model.http.ClientType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface HttpClient {

    int threadNumber() default 4;

    int timeout() default 3_000;

    boolean enableCookie() default false;

    ClientType clientType() default ClientType.OK_HTTP_CLIENT;

    boolean enableSsl() default true;

    boolean followRedirects() default true;

    boolean privateClient() default false;

}
