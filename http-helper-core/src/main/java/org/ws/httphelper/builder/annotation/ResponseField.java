package org.ws.httphelper.builder.annotation;


import org.ws.httphelper.model.field.ExpressionType;
import org.ws.httphelper.model.field.ParseFieldType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 响应参数
 * 1.Annotations are compile-time constants.
 * 2.Annotation members can only be compile-time constants (Strings, primitives, enums, annotations, class literals).
 * 3.Anything that references itself can't be a constant, so an annotation can't reference itself.
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseField {

    String fieldName() default "";

    ParseFieldType fieldType() default ParseFieldType.STRING;

    String parseExpression() default "";

    ExpressionType expressionType() default ExpressionType.CSS;

    ChildrenField[] childrenField() default {};
}
